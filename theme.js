// @flow
import { DefaultTheme } from 'react-native-paper';

export const theme = {
  ...DefaultTheme,
  roundness: 5,
  paddingHorizontal: {
    card: 15,
  },
  colors: {
    ...DefaultTheme.colors,
    primary: '#0e3563',
    accent: '#051e41',
    link: '#00ccff',
    surface: '#ffffff',
    inactive: '#fff2db',
    inactiveTint: '#999',
    background: '#e9e9e9',
    bottomBorder: '#d8d8d8',
    success: '#65af01',
    navigation: '#5983e8',
    secondary: '#B22222',
    text: '#000',
    high: '#B22222',
    average: '#fda50f',
    low: '#0A6522',
  },
  fonts: {
    regular: 'Roboto-Regular',
    light: 'Roboto-Light',
    medium: 'Roboto-Bold',
    thin: 'Roboto-Thin',
  }
};
