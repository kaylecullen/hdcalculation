/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, View } from 'react-native';
import Navigator from './src/utils/routes';
import { Provider as PaperProvider, Portal, Dialog, ActivityIndicator, Text, Button } from 'react-native-paper';
import { Provider, Subscribe } from 'unstated';
import { AppContainer, appContainer } from './src/container';
import { theme } from './theme';
import { getStorageValue, TOKEN, UNAME } from './src/utils/auth';
type Props = {};
export default class App extends Component<Props> {
  constructor(props){
    super(props);
  }

  async componentDidMount(){
    await getStorageValue(UNAME).then(res => {
      appContainer.setState({ username: res });
    });
  }

	render() {
		return (
      <Provider inject={[ appContainer ]}>
        <Subscribe to={[ AppContainer ]}> 
          {( appStore ) => (
            <PaperProvider>
              <Navigator />

              <Portal>
                {/* loading dialog */}
                <Dialog visible={appStore.state.isLoading}>
                  <Dialog.Content>
                    <View style={{ paddingVertical: 5 }}>
                      <ActivityIndicator animating={true} color={theme.colors.primary} size='large'/>
                    </View>
                  </Dialog.Content>
                </Dialog>

                {/* error dialog */}
                <Dialog visible={appContainer.state.showError}>
                  <Dialog.Title>Error</Dialog.Title>
                  <Dialog.Content>
                    {/* <Text>{appStore.state.errorMessage}</Text> */}
                    <Text>{appStore.state.errorMessage}</Text>
                  </Dialog.Content>
                  <Dialog.Actions>
                    <Button onPress={() => appContainer.setState({ showError: false, errorMessage: null})}>OK</Button>
                  </Dialog.Actions>
                </Dialog>
              </Portal>
            </PaperProvider>
          )}
        </Subscribe>
      </Provider>
    )
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF'
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5
	}
});
