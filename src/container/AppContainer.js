import { Container } from "unstated";
import { fetchUrl } from '../utils/fetch';
import { endpoint } from '../utils/endpoints';
import { getStorageValue, UNAME, TOKEN, setStorageValue, RISK_LEVEL, PERCENTAGE_RISK } from '../utils/auth';

const initialState = {
	isLoading: false,
	showError: false,
	username: null,
	errorMsg: null,
	hasError: false,
	riskRate: null,
  percentageRisk: null,
  age: null,
  userData: null,
  disableRegister: false,
  disableLogin: false,
  disableButton: false,
  disablePassword: false,
  calculatorValues: {
    gender: null,
    smoker: null,
    diabetes: null,
    blood_pressure: null,
    ldl: null,
    hdl: null,
    age: null,
    exercise_daily: null,
    taking_medicine: null,
    eating_habits: null,
    alcohol: null,
    house_smoke: null,
    other_activity: null,
  }
};
class AppContainer extends Container {
  state = initialState;
  
  getHistory = async (uname) => {
    const userDetails = await fetchUrl(
      endpoint.userStatus + "/" + uname,
      "GET"
    );

    return userDetails;
  }

  calculateForm = async() => {
    this.setState({ isLoading: true, disableButton: true });
    const totalValue = await this.computeAll();
    const percentage = await this.computePercentage(totalValue);
    const riskRate = await this.getRiskLevel(percentage);

    const username = await getStorageValue(UNAME).then(res => { return res});
    const checkToken = await getStorageValue(TOKEN).then(res => {
      if(res){
        return true;
      }
      return true;
    });

    if(checkToken){
      const data = this.state.calculatorValues;
      const formatData = {
        Username: username,
        Age: parseInt(data.age),
        BloodPressure: data.blood_pressure,
        LDLLevel: data.ldl,
        HDLLevel: data.hdl,
        Gender: data.gender,
        Smoker: data.smoker === 'Yes' ? true: false,
        Diabetes: data.diabetes === 'Yes' ? true : false,
        RegularExercise: data.exercise_daily === 'Yes' ? true : false,
        HeartAge: parseInt(data.age),
        CardioPercRisk: percentage,
        RiskLevel: riskRate,
        EatingHabits: data.eating_habits === 'Yes' ? true : false,
        Alcohol: data.alcohol === 'Yes' ? true : false,
        HouseSmoker: data.house_smoke === 'Yes' ? true : false,
        OtherActivity: data.other_activity,
        // CreatedDate: new Date().toLocaleDateString(),        
      };

      console.log(formatData);
      const saveData = await fetchUrl(
        endpoint.heartCalculator,
        "POST",
        JSON.stringify(formatData)
      );

      if(saveData === 'Success'){
        await setStorageValue(RISK_LEVEL, riskRate);
        await setStorageValue(PERCENTAGE_RISK, percentage);
      }
    }

    await this.setState({ 
      riskRate: riskRate, 
      percentageRisk: percentage,
      userData: this.state.calculatorValues,
      age: this.state.calculatorValues.age
    });

    const returnData = {
      percentage: percentage,
      age: this.state.calculatorValues.age,
      riskRate: riskRate,
    }

    this.setState({ isLoading: false})

    return JSON.stringify(returnData);

    // this.setState({ user: initialState });
  }

  computeAll = async () => {
    const ageValue = await this.getAgeValue();
    const bpValue = await this.getBloodPressure();
    const ldlValue = await this.getLdlValue();
    const hdlValue = await this.getHdlValue();
    const diabetesValue = await this.getDiabetesValue();
    const exerciseValue = await this.getExerciseValue();
    const smokerValue = await this.getSmokerValue();
    const dietValue = await this.getDietValue();
    const alcoholValue = await this.getAlcoholValue();
    const smokerHouse = await this.getSmokerHouse();
    const otherActivity = await this.getOtherActivities();

    let totalValue = 
      ageValue + 
      bpValue + 
      ldlValue +
      hdlValue +
      diabetesValue +
      exerciseValue +
      smokerValue +
      dietValue +
      alcoholValue +
      smokerHouse +
      otherActivity;

      console.log("totalValue", totalValue);
      // console.log(ageValue, bpValue, ldlValue, hdlValue, diabetesValue, exerciseValue, smokerValue, dietValue, alcoholValue, smokerHouse, otherActivity);

      return totalValue;
  }

  computePercentage = (totalValue) => {
    const { gender } = this.state.calculatorValues;
    let percentage;
    if(gender === 'Male'){
      if(totalValue <= -3 ){
        percentage = 1;
      }else if(totalValue === -2 || totalValue === -1){
        percentage = 2;
      }else if(totalValue === 0){
        percentage = 3;
      }else if(totalValue === 1 || totalValue === 2){
        percentage = 4;
      }else if(totalValue === 3){
        percentage = 6;
      }else if(totalValue === 4){
        percentage = 7;
      }else if(totalValue === 5){
        percentage = 9;
      }else if(totalValue === 6){
        percentage = 11;
      }else if(totalValue === 7){
        percentage = 14;
      }else if(totalValue === 8){
        percentage = 18;
      }else if(totalValue === 9){
        percentage = 22;
      }else if(totalValue === 10){
        percentage = 27;
      }else if(totalValue === 11){
        percentage = 33;
      }else if(totalValue === 12){
        percentage = 40;
      }else if(totalValue === 13){
        percentage = 47;
      }else if( totalValue >= 14){
        percentage = 56;
      }
    }else if(gender === 'Female'){
      if( totalValue <= -2){
        percentage = 1;
      }else if(totalValue === -1 || totalValue === 0 || totalValue === 1){
        percentage = 2;
      }else if(totalValue === 2 || totalValue === 3){
        percentage = 3;
      }else if(totalValue === 4){
        percentage = 4;
      }else if(totalValue === 5){
        percentage = 5;
      }else if(totalValue === 6){
        percentage = 6;
      }else if(totalValue === 7){
        percentage = 7;
      }else if(totalValue === 8){
        percentage = 8;
      }else if(totalValue === 9){
        percentage = 9;
      }else if(totalValue === 10){
        percentage = 11;
      }else if(totalValue === 11){
        percentage = 13;
      }else if(totalValue === 12){
        percentage = 15;
      }else if(totalValue === 13){
        percentage = 17;
      }else if(totalValue === 14){
        percentage = 20;
      }else if(totalValue === 15){
        percentage = 24;
      }else if(totalValue === 16){
        percentage = 27;
      }else if(totalValue >= 17){
        percentage = 32;
      }
    }
    console.log("percentage", percentage)
    return percentage;
  }

  getRiskLevel = (percentage) => {
    let riskRate;

    if(percentage <= 10){
      riskRate = 'Low';
    }else if(percentage > 10 && percentage <= 50){
      riskRate = 'Average';
    }else{
      riskRate = 'High';
    }

    console.log("riskRate",riskRate);
    return riskRate;
  }

  getAgeValue = () => {
    const { gender, age } = this.state.calculatorValues;

    let ageValue;

    if(gender === 'Female'){
      if( age <= 29 ){
        ageValue = -10;
      }else if( age >= 30 && age <= 34) {
        ageValue = -9;
      }else if( age >= 35 && age <= 39) {
        ageValue = -4;
      }else if( age >= 40 && age <= 44){
        ageValue = 0;
      }else if( age >= 45 && age <= 49){
        ageValue = 3;
      }else if( age >= 50 && age <= 54){
        ageValue = 6;
      }else if( age >= 55 && age <= 59){
        ageValue = 7;
      }else if( age >= 60 && age <= 74){
        ageValue = 8;
      }else if( age > 74){
        ageValue = 9;
      }
    }else if(gender === 'Male'){
      if( age <= 29 ) {
        ageValue = -2;
      }else if( age >= 30 && age <= 34){
        ageValue = -1;
      }else if( age >= 35 && age <= 69){
        ageValue = 3;
      }else if( age >= 70 && age <= 74){
        ageValue = 7;
      }else if( age >= 75) {
        ageValue = 8;
      }
    }

    console.log("ageValue", ageValue);
    return ageValue;
  } 

  getBloodPressure = () => {
    const { blood_pressure, gender } = this.state.calculatorValues;
    console.log(this.state.calculatorValues.blood_pressure);
    let bpValue;

    if(gender === 'Female'){
      if( blood_pressure <= 119 ) {
        bpValue = -3;
      }else if(blood_pressure >= 120 && blood_pressure <= 139){
        bpValue = 0;
      }else if(blood_pressure >= 140 && blood_pressure <= 159){
        bpValue = 2;
      }else if( blood_pressure >= 160 ){
        bpValue = 3;
      }
    }else if(gender === 'Male'){
      if( blood_pressure <= 129){
        bpValue = 0;
      }else if( blood_pressure >= 130 && blood_pressure <= 139){
        bpValue = 1;
      }else if( blood_pressure >= 140 && blood_pressure <= 159){
        bpValue = 2;
      }else if( blood_pressure >= 160){
        bpValue = 3;
      }
    }

    console.log("bpValue", bpValue);
    return bpValue;
  }

  getLdlValue = () => {
    const { gender, ldl } = this.state.calculatorValues;
    let ldlValue;
    if(gender === 'Female'){
      if( ldl <= 99 ) {
        ldlValue = -2;
      }else if( ldl >= 100 && ldl <= 159 ){
        ldlValue = 0;
      }else if( ldl >= 160 && ldl <= 190){
        ldlValue = 0;
      }else if(ldl > 190){
        ldlValue = 2;
      }
    }else if(gender === 'Male'){
      if( ldl <= 99 ){
        ldlValue = -3;
      }else if(ldl >= 100 && ldl <= 159){
        ldlValue = 0;
      }else if(ldl >= 160 && ldl <= 190){
        ldlValue = 1;
      }else if(ldl > 190){
        ldlValue = 2;
      }
    }

    console.log("ldlValue", ldlValue);
    return ldlValue;
  }

  getHdlValue = () => {
    const { gender, hdl} = this.state.calculatorValues;
    let hdlValue;

    if(gender === 'Female'){
      if( hdl <= 34 ){
        hdlValue = 5;
      }else if(hdl >= 35 && hdl <= 44){
        hdlValue = 2;
      }else if( hdl >= 45 && hdl <= 49){
        hdlValue = 1;
      }else if( hdl >= 50 && hdl <= 59){
        hdlValue = 0;
      }else if( hdl >= 60) {
        hdlValue = -2;
      }
    }else if(gender === 'Male'){
      if( hdl <= 34 ){
        hdlValue = 2;
      }else if(hdl >= 35 && hdl <= 44){
        hdlValue = 1;
      }else if(hdl >= 45 && hdl <= 59){
        hdlValue = 0;
      }else if(hdl >= 60){
        hdlValue = -1;
      }
    }

    console.log("hdlValue", hdlValue);
    return hdlValue;
  }

  getDiabetesValue = () => {
    const { diabetes, gender } = this.state.calculatorValues;
    let diabetesValue;

    if(gender === 'Female'){
      if( diabetes === 'Yes'){
        diabetesValue = 4;
      }else{
        diabetesValue = 0;
      }
    }else if(gender === 'Male'){
      diabetesValue = 0;
    }

    console.log("diabetesValue", diabetesValue);
    return diabetesValue;
  }

  getExerciseValue = () => {
    const { exercise_daily } = this.state.calculatorValues;
    let exerciseDaily;

    if( exerciseDaily === 'Yes'){
      exerciseDaily = -1;
    }else{
      exerciseDaily = 1;
    }

    console.log("exerciseDaily", exerciseDaily);
    return exerciseDaily;
  }

  getSmokerValue = () => {
    const { smoker } = this.state.calculatorValues;
    let isSmoker;

    if( smoker === 'Yes'){
      isSmoker = 2;
    }else{
      isSmoker = 0;
    }

    console.log("isSmoker", isSmoker);
    return isSmoker;
  }

  getDietValue = () => {
    const { eating_habits } = this.state.calculatorValues;
    let healthyDiet;

    if( eating_habits === 'Yes'){
      healthyDiet = -1;
    }else{
      healthyDiet = 1;
    }

    console.log("healthyDiet", healthyDiet);
    return healthyDiet;
  }

  getAlcoholValue = () => {
    const { alcohol } = this.state.calculatorValues;
    let alcoholValue;

    if(alcohol === 'Yes'){
      alcoholValue = 2;
    }else{
      alcoholValue = 0;
    }
    
    console.log("alcoholValue", alcoholValue);
    return alcoholValue;
  }

  getSmokerHouse = () => {
    const { house_smoke } = this.state.calculatorValues;
    let smokerInHouse;

    if(house_smoke === 'Yes'){
      smokerInHouse = 2;
    }else{
      smokerInHouse = 0;
    }

    console.log("smokerInHouse", smokerInHouse);
    return smokerInHouse;
  }

  getOtherActivities = () => {
    const { other_activity} = this.state.calculatorValues;
    let otherActivity;
    
    if(other_activity !== 'none'){
      otherActivity = 0;
    }else{
      otherActivity = 2;
    }

    console.log("otherActivity", otherActivity);
    return otherActivity;
  }

  clearState = () => {
    this.setState({initialState});
  }
}

export { AppContainer };
