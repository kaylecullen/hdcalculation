import { AuthContainer } from './AuthContainer';
import { AppContainer } from './AppContainer';

export const authContainer = new AuthContainer();
export const appContainer = new AppContainer();

export { AuthContainer, AppContainer };