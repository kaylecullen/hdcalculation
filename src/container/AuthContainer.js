import { Container } from "unstated";
import { endpoint } from "../utils/endpoints";
import { fetchUrl } from "../utils/fetch";
import { appContainer } from "../container";
import { TOKEN, setStorageValue, UNAME, RISK_LEVEL, PERCENTAGE_RISK } from "../utils/auth";
import { AsyncStorage } from 'react-native';
const initialState = {
	// errorMsg = null,
};

class AuthContainer extends Container {
	state = initialState;

	loginRequest = async data => {
		appContainer.setState({ isLoading: true, disableLogin: true });
		const formatData =
			"grant_type=password&username=" +
			data.username +
			"&password=" +
			data.password;

		const isLoggedIn = await fetchUrl(
			endpoint.login,
			"POST",
			formatData,
			"application/x-www-form-urlencoded"
    );
    
    console.log(isLoggedIn);


		if(isLoggedIn === 'error'){
      appContainer.setState({ isLoading: false, disableLogin: false })
    }else{
      if (isLoggedIn.hasOwnProperty("error")) {

        appContainer.setState({
          errorMsg: isLoggedIn.error_description,
          hasError: true
        });
        
        appContainer.setState({ isLoading: false, disableLogin: false });
      } else {
        const token = await setStorageValue(TOKEN, isLoggedIn.access_token);
        const uname = await setStorageValue(UNAME, isLoggedIn.userName);
        appContainer.setState({ isLoading: false, disableLogin: true, errorMsg: null, hasError: false });
        
        return true;
      }
    }
  };
  
	registerRequest = async data => {
		appContainer.setState({ isLoading: true, disableRegister: true });

		const formatData = {
			Username: data.username,
			Email: data.email,
			Password: data.password,
      ConfirmPassword: data.repeat_password,
		};

		const isRegistered = await fetchUrl(
			endpoint.register,
			"POST",
			JSON.stringify(formatData)
		);

    console.log("isRegistered", isRegistered);

    if(isRegistered === 'error'){
      let errors = [];
      await this.setState({ networkError: false });
      await appContainer.setState({ isLoading: false, disableRegister: false });
      return errors;
    }else{
      if(isRegistered.hasOwnProperty('ModelState')){
        let errors = [];
        appContainer.setState({ isLoading: false, disableRegister: false });
        const key = "model.ConfirmPassword";
        if(isRegistered.ModelState[key]){
          // return ["repeat_password"];
          errors.push("repeat_password_invalid");
        }
        const key_pass = "model.Password"
        if(isRegistered.ModelState[key_pass]){
          errors.push("password_invalid");
        }
  
        if(isRegistered.ModelState[""]){
          isRegistered.ModelState[""].map(val => {
            if(val.includes("Name")){
              errors.push("username_invalid")
            }
  
            if(val.includes("Email")){
              errors.push("email_existing");
            }
  
            if(val.includes("Passwords")){
              errors.push("password_invalid");
            }
          })
        }
        return errors;
      }
    }

    if(isRegistered === "Success"){
      appContainer.setState({ isLoading: false, disableRegister: true });
			return true;
    }

    // if(isRegistered){
      
    // }else if(isRegistered === 'error'){
    //   let errors = [];
    //   await this.setState({ networkError: false });
    //   await appContainer.setState({ isLoading: false, disableRegister: false });
    //   return errors;
    // }else if(isRegistered === "Success"){
    //   appContainer.setState({ isLoading: false, disableRegister: true });
		// 	return true;
    // }
    
    // if(isRegistered === 'error'){
    //   let errors = [];
    //   await this.setState({ networkError: false });
    //   await appContainer.setState({ isLoading: false, disableRegister: false });
    //   return errors;
    // }

		// if (isRegistered === "Success") {
		// 	appContainer.setState({ isLoading: false, disableRegister: true });
		// 	return true;
    // }
    
    
		//register request from api
	};

	changePassword = async data => {
		appContainer.setState({ isLoading: true, disablePassword: true });
		const formatData = {
			"OldPassword": data.current,
			"NewPassword": data.password,
			"ConfirmPassword": data.retype
    };
    
    const changeSuccess = await fetchUrl(endpoint.changePassword, "POST", JSON.stringify(formatData));

    let errors = [];
    if(changeSuccess === 'error'){
      await appContainer.setState({ isLoading: false, disablePassword: false });
      return errors;
    }else{
      if(changeSuccess.hasOwnProperty('ModelState')){
        appContainer.setState({ isLoading: false, disablePassword: false });
  
        if(changeSuccess.ModelState[""]){
          errors.push("current_invalid")
        }
    
        const key_password = 'model.NewPassword';
        const key_confirm = 'model.ConfirmPassword';
    
        if(changeSuccess.ModelState[key_password]){
          errors.push("password_invalid");
        }
    
        if(changeSuccess.ModelState[key_confirm]){
          errors.push("confirm_invalid");
        }
  
        return errors;
      }
    }

    
    if(changeSuccess === 'success'){
      appContainer.setState({ isLoading: false, disablePassword: false });
      return true;
    }
	};
}

export const onLogout = (props) => {
  AsyncStorage.removeItem(TOKEN);
	AsyncStorage.removeItem(UNAME);
	AsyncStorage.removeItem(PERCENTAGE_RISK);
	AsyncStorage.removeItem(RISK_LEVEL);

}

export { AuthContainer };
