import React, { PureComponent } from "react";
import { ScrollView, View, Dimensions, AsyncStorage } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { Form, Container, Content } from "native-base";
import { FormInput, CustomButton } from "../components";
import { strings } from "../utils/strings";
import { theme } from "../../theme";
import { Text } from "react-native-paper";
import {
	authContainer,
	AuthContainer,
	appContainer,
	AppContainer
} from "../container";
import { Provider, Subscribe } from "unstated";

const initialState = {
	username: "",
	email: "",
	password: "",
	repeat_password: ""
};
class Register extends PureComponent {
	constructor() {
		super();
		this.state = {
			user: initialState,
			errors: [],
			showPassword: false,
			showRetype: false
		};
	}

	static navigationOptions = ({ navigation }) => ({
		title: strings.back,
		headerStyle: {
			backgroundColor: theme.colors.navigation
		},
		headerTintColor: theme.colors.surface
	});

	_onRegisterSubmit = async () => {
		// clear state per login click
		this.setState({ errors: [] });

		//push to error state if property is null or empty
		const checkErrors = await Object.entries(this.state.user).map(val => {
			if (!val[1]) {
				this.setState(prevState => ({
					errors: [...prevState.errors, val[0]]
				}));
			} else {
			}
		});

		const validateEmail = await this.validateField(
			"email",
			this.state.user.email
		);
		if (!validateEmail) {
			this.setState(prevState => ({
				errors: [...prevState.errors, "email_invalid"]
			}));
		}

		if (this.state.errors.length <= 0) {
			//save to db
			const isRegistered = await authContainer.registerRequest(this.state.user);

			if (isRegistered === true) {
				await setTimeout(() => this.registerSuccess(), 3000);
			} else {
				this.setState({ errors: isRegistered });
			}
		}
	};

	registerSuccess = () => {
    appContainer.setState({ isLoading: false, disableRegister: false });
    this.props.navigation.replace("Login");
    this.setState({ user: initialState });
	};

	validateField = (type, value) => {
		const email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		let check_format;

		switch (type) {
			case "email":
				check_format = email_regex.test(value);
				break;
			default:
				break;
		}
		return check_format;
	};

	render() {
		const { height } = Dimensions.get("window");
		return (
			// <LinearGradient
			// 	style={{ flex: 1, height: height }}
			// 	start={{ x: 0.75, y: 0 }}
			// 	end={{ x: 1, y: 0.75 }}
			// 	colors={[
			// 		theme.colors.primary,
			// 		theme.colors.primary,
			// 		theme.colors.accent
			// 	]}
			// >
			<Provider inject={[appContainer]}>
				<Subscribe to={[AppContainer]}>
					{appStore => (
						<ScrollView>
							<View style={{ marginVertical: 10, marginHorizontal: 20 }}>
								<View style={{ paddingHorizontal: 20, paddingVertical: 20 }}>
									<Text
										style={{
											fontSize: 40,
											letterSpacing: 1,
											color: theme.colors.text
										}}
									>
										{strings.register}
									</Text>
								</View>
								<Form>
									<FormInput
										label={strings.username}
										onValueChange={value =>
											this.setState({
												user: { ...this.state.user, username: value }
											})
										}
										value={this.state.user.username}
										errorVisible={
											this.state.errors.includes("username") ||
											this.state.errors.includes("username_invalid")
										}
										errorMessage={
											this.state.errors.includes("username")
												? strings.username_error
												: strings.username_invalid
										}
									/>
									<FormInput
										label={strings.email}
										onValueChange={value =>
											this.setState({
												user: { ...this.state.user, email: value }
											})
										}
										value={this.state.user.email}
										errorVisible={
											this.state.errors.includes("email") ||
											this.state.errors.includes("email_invalid") ||
											this.state.errors.includes("email_existing")
										}
										errorMessage={
											this.state.errors.includes("email_existing")
												? strings.email_existing
												: this.state.errors.includes("email_invalid")
												? strings.email_invalid
												: strings.email_error
										}
									/>
									<FormInput
										label={strings.password}
										onValueChange={value =>
											this.setState({
												user: { ...this.state.user, password: value }
											})
										}
										value={this.state.user.password}
										isPassword={true}
										showPassword={() =>
											this.setState({
												showPassword: !this.state.showPassword
											})
										}
										isShown={this.state.showPassword}
										errorVisible={
											this.state.errors.includes("password") ||
											this.state.errors.includes("password_invalid")
										}
										errorMessage={
											this.state.errors.includes("password_invalid")
												? strings.password_invalid
												: strings.password_error
										}
									/>
									<FormInput
										label={strings.repeat_password}
										onValueChange={value =>
											this.setState({
												user: { ...this.state.user, repeat_password: value }
											})
										}
										value={this.state.user.repeat_password}
										isPassword={true}
										showPassword={() =>
											this.setState({ showRetype: !this.state.showRetype })
										}
										isShown={this.state.showRetype}
										errorVisible={
											this.state.errors.includes("repeat_password") ||
											this.state.errors.includes("repeat_password_invalid")
										}
										errorMessage={
											this.state.errors.includes("repeat_password_invalid")
												? strings.confirm_password_invalid
												: strings.password_error
										}
									/>
								</Form>
								<View style={{ paddingVertical: 20 }}>
									<CustomButton
										label={strings.register}
										full
										onPress={() => this._onRegisterSubmit()}
										disabled={appStore.state.disableRegister}
									/>
								</View>
								<Text
									style={{
										color: theme.colors.text,
										alignSelf: "center",
										marginVertical: 8,
										fontSize: 12
									}}
									onPress={() => this.props.navigation.navigate("Login")}
								>
									{strings.have_an_account}
								</Text>
							</View>
						</ScrollView>
					)}
				</Subscribe>
			</Provider>
			// </LinearGradient>
		);
	}
}

export default Register;
