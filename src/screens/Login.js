import React, { PureComponent } from "react";
import { View } from "react-native";
import { strings } from "../utils/strings";
import { Form, Item, Input, Label } from "native-base";
import LinearGradient from "react-native-linear-gradient";
import { FormInput, CustomButton } from "../components";
import { theme } from "../../theme";
import { Text, HelperText } from "react-native-paper";
import { authContainer, appContainer, AppContainer } from "../container";
import { Provider, Subscribe } from "unstated";

class Login extends PureComponent {
	constructor() {
		super();

		this.state = {
			user: {
				username: "",
				password: ""
			},
			errors: [],
			showPassword: false
		};
	}

	componentWillUnmount() {
		appContainer.setState({ hasError: false, errorMsg: null });
	}

	static navigationOptions = ({ navigation }) => ({
		title: strings.back,
		headerStyle: {
			backgroundColor: theme.colors.navigation
		},
    headerTintColor: theme.colors.surface,
	});

	_onLoginSubmit = async () => {
		//clear state per login click
		this.setState({ errors: [] });

		//push to error state if property is null or empty
		const pushErrors = await Object.entries(this.state.user).map(val => {
			if (!val[1]) {
				this.setState(prevState => ({
					errors: [...prevState.errors, val[0]]
				}));
			}
		});

		if (!this.state.errors.length > 0) {
			//TODO: call function for login request
			const isLoggedIn = await authContainer.loginRequest(this.state.user);
			if (isLoggedIn) {
				// const token = await setStorageValue(TOKEN, this.state.user.username);
				await setTimeout(() => this.navigateToLogin(), 3000);
			}
		} else {
			await appContainer.setState({
				errorMsg: "Please fill up empty fields",
				hasError: true
			});
		}
	};

	navigateToLogin = () => {
		appContainer.setState({ isLoading: false, disableLogin: false });
		this.props.navigation.navigate("App");
	};

	render() {
		return (
			// <LinearGradient
			// 	style={{ flex: 1 }}
			// 	start={{ x: 0.75, y: 0 }}
			// 	end={{ x: 1, y: 0.75 }}
			// 	colors={[
			// 		theme.colors.primary,
			// 		theme.colors.primary,
			// 		theme.colors.accent
			// 	]}
			// >
				<Provider inject={[appContainer]}>
					<Subscribe to={[AppContainer]}>
						{appStore => (
							<View style={{ flex: 1, justifyContent: "center" }}>
								<View style={{ marginVertical: 10, marginHorizontal: 20 }}>
									<View style={{ paddingHorizontal: 20, paddingVertical: 30 }}>
										<Text
											style={{ fontSize: 40, letterSpacing: 1, color: "#000" }}
										>
											{strings.login}
										</Text>
									</View>
									<Form>
										<FormInput
											label={strings.username}
											onValueChange={value =>
												this.setState({
													user: { ...this.state.user, username: value }
												})
											}
											value={this.state.user.username}
										/>
										<FormInput
											label={strings.password}
											onValueChange={value =>
												this.setState({
													user: { ...this.state.user, password: value }
												})
											}
											value={this.state.user.password}
											isPassword={true}
											showPassword={() =>
												this.setState({
													showPassword: !this.state.showPassword
												})
											}
											isShown={this.state.showPassword}
										/>
										<HelperText
											type="error"
											visible={appStore.state.hasError}
											style={{ color: theme.colors.secondary }}
										>
											{appStore.state.errorMsg}
										</HelperText>
									</Form>
									<View style={{ paddingVertical: 25 }}>
										<CustomButton
											label={strings.login}
											full
                      onPress={() => this._onLoginSubmit()}
                      disabled={appStore.state.disableLogin}
										/>
									</View>
								</View>
							</View>
						)}
					</Subscribe>
				</Provider>
			// </LinearGradient>
		);
	}
}

export default Login;
