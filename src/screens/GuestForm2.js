import React, { PureComponent } from "react";
import { View, ScrollView, TouchableOpacity } from "react-native";
import { Text, RadioButton, Caption, HelperText } from "react-native-paper";
import { Left } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { appContainer } from "../container";
import { strings } from "../utils/strings";
import { theme } from "../../theme";
import NumericInput from "react-native-numeric-input";
import { RadioButton as Radio, CustomButton, FormInput } from "../components";
import SFNumberPicker from "react-native-sf-numberpicker";
import { StackActions, NavigationActions } from 'react-navigation';

initialState = {
	blood_pressure: null,
	taking_medicine: null
};
class GuestForm2 extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			user: initialState,
			errors: []
		};

		let value;
	}

	componentWillUnmount() {
		this.setState({ user: initialState });
	}

	onBloodPressurePicker = value => {
		this.value = value;
		this.setState(
			{
				user: {
					...this.state.user,
					blood_pressure: value
				}
			},
			() => console.log(this.state.user.blood_pressure)
		);
	};

	onFormSubmit = async () => {
		let data = {
			blood_pressure: this.state.user.blood_pressure,
			taking_medicine: this.state.user.taking_medicine
		};
		this.setState({ errors: [] });

		const pushErrors = await Object.entries(data).map(val => {
			if (!val[1]) {
				this.setState(prevState => ({
					errors: [...prevState.errors, val[0]]
				}));
			}
		});

		if (this.state.errors.length <= 0) {
			await appContainer.setState(
				{
					calculatorValues: {
						...appContainer.state.calculatorValues,
						...data
					}
				},
				() => {
					this.props.navigation.navigate("GuestForm3");
				}
			);
		}
	};

	render() {
		return (
			<ScrollView>
				<View style={{ marginHorizontal: 20 }}>
					<View style={{ paddingVertical: 15 }}>
						<Text style={{ fontSize: 16, fontWeight: "bold" }}>
							Blood Pressure
						</Text>
					</View>
					<View style={{ paddingVertical: 5 }}>
						{/* <Text style={{ fontSize: 16 }}>Actual Systolic:</Text> */}
						<View style={{ paddingHorizontal: 20 }}>
							{/* <SFNumberPicker 
                width={200} 
                height={50} 
                maxNumber={250} 
                onNumberChanged={ (tag, value) => this.setState({ user: { ...this.state.user, blood_pressure: value}})} 
                fontSize={16}
                value={this.value}
              />
              <Text>{this.state.user.blood_pressure}</Text> */}
							<FormInput
								label={"Actual Systolic"}
								onValueChange={value =>
									this.setState({
										user: { ...this.state.user, blood_pressure: value }
									})
								}
                value={this.state.user.blood_pressure}
                isNumber={true}
                hasUnit = {"mmHg"}
							/>
						</View>
						<Text>OR</Text>
						<View style={{ paddingVertical: 5 }}>
							<RadioButton.Group
								onValueChange={value => this.onBloodPressurePicker(value)}
								value={this.state.user.blood_pressure}
							>
								<View
									style={{
										flexDirection: "row",
										justifyContent: "space-between",
										alignSelf: "flex-start",
										paddingHorizontal: 20
									}}
								>
									<RButton title="Low" value={"110"} />
									<RButton title="Normal" value={"125"} />
								</View>
								<View
									style={{
										flexDirection: "row",
										justifyContent: "space-between",
										alignSelf: "flex-start",
										paddingHorizontal: 20
									}}
								>
									<RButton title="High" value={"155"} />
									<RButton title="Don't know" value={"120"} />
								</View>
							</RadioButton.Group>
						</View>

						{/* error message if null */}
						<HelperText
							type="error"
							visible={this.state.errors.includes("blood_pressure")}
							style={{ color: theme.colors.secondary }}
						>
							Field is required! Please fill up!
						</HelperText>
					</View>
					<View style={{ paddingVertical: 15 }}>
						<Radio
							data={["Yes", "No"]}
							label={"Are you taking medicine for high blood pressure?"}
							selected={this.state.user.taking_medicine}
							onValueChange={selected =>
								this.setState({
									user: { ...this.state.user, taking_medicine: selected }
								})
							}
							errorVisible={this.state.errors.includes("taking_medicine")}
							errorMessage={strings.empty_field}
						/>
					</View>
					<View>
						<Caption>
							Enter your systolic (top number) blood pressure in the box, or if
							your doctor told you your blood pressure is low, normal, or high,
							then choose that. If you don’t know your blood pressure, choose
							Don’t Know, but the results won’t be as personal to you.{" "}
						</Caption>
					</View>
					<View
						style={{
							paddingVertical: 20,
							flexDirection: "row",
							justifyContent: "center"
						}}
					>
						<CustomButton
							label={"Previous"}
							onPress={() => this.props.navigation.navigate("Guest")}
						/>
						<CustomButton label={"Next"} onPress={() => this.onFormSubmit()} />
					</View>
				</View>
			</ScrollView>
		);
	}
}

const RButton = props => (
	<View style={{ flexDirection: "row", paddingHorizontal: 30 }}>
		<RadioButton
			value={props.value}
			color={theme.colors.text}
			uncheckedColor={"#ccc"}
		/>
		<Text style={{ alignSelf: "center", fontSize: 16 }}>{props.title}</Text>
	</View>
);

export default GuestForm2;
