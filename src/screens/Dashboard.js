import React, { PureComponent } from "react";
import {
	View,
	Text,
	TouchableOpacity,
	Dimensions,
	ScrollView,
	StyleSheet,
	FlatList,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { Left } from "native-base";
import { strings } from "../utils/strings";
import { getStorageValue, RISK_LEVEL, PERCENTAGE_RISK } from "../utils/auth";
import { Provider, Subscribe } from "unstated";
import { appContainer, AppContainer } from "../container";
import { theme } from "../../theme";
import { fetchUrl } from "../utils/fetch";
import { endpoint } from "../utils/endpoints";
class Dashboard extends PureComponent {
	state = {
		riskRate: null,
		percentageRisk: null,
    healthTips: [],
    networkError: false,
	};

	static navigationOptions = ({ navigation }) => ({
		title: strings.dashboard,
		headerStyle: {
			backgroundColor: theme.colors.navigation
		},
		headerTintColor: theme.colors.surface,
		headerLeft: (
			<Left>
				<TouchableOpacity onPress={() => navigation.openDrawer()}>
					<Icon
						name="bars"
						style={{ color: "#fff", fontSize: 20, paddingLeft: 15 }}
					/>
				</TouchableOpacity>
			</Left>
		)
	});

	async componentDidMount() {
		const risk_rate = await getStorageValue(RISK_LEVEL).then(res => {
			return res;
		});
		const percentage_risk = await getStorageValue(PERCENTAGE_RISK).then(res => {
			return res;
		});

		await this.setState({
			riskRate: risk_rate,
			percentageRisk: percentage_risk
		});

		await appContainer.setState({
			riskRate: risk_rate,
			percentageRisk: percentage_risk
		});

		let value =
			appContainer.state.riskRate === "Average" ||
			appContainer.state.riskRate === "Normal"
				? "Normal"
				: appContainer.state.riskRate;

    const level = await fetchUrl(endpoint.percentageLevel + value, "GET");
    
    if(level === 'error'){
      this.setState({ networkError: true});
    }else{
      await this.setState({ healthTips: level, networkError: false });
    }

		
	}

	render() {
		const { width } = Dimensions.get("window");
		const dataTips =
			appContainer.state.riskRate === "High"
				? this.state.highTips
				: appContainer.state.riskRate === "Average"
				? this.state.averageTips
				: appContainer.state.riskRate === "Low"
				? this.state.lowTips
				: this.state.normalTips;
		return (
			<ScrollView style={{ flex: 1 }}>
				<Provider inject={[appContainer]}>
					<Subscribe to={[AppContainer]}>
						{appStore => (
							<View style={{ padding: 30 }}>
								<View style={[styles.container]}>
									<View style={{ justifyContent: "center" }}>
										<Text style={[styles.dashText]}>Current</Text>
										<Text style={[styles.dashText]}>Risk</Text>
										<Text style={[styles.dashText]}>Level</Text>
									</View>
									<View>
										<View style={[styles.circleContent]}>
											<View
												style={{ alignSelf: "center", alignItem: "center" }}
											>
												<Text
													style={{
														fontSize: 30,
														color:
															appStore.state.riskRate === "High"
																? theme.colors.high
																: appStore.state.riskRate === "Average"
																? theme.colors.average
																: theme.colors.low,
														paddingVertical: 50
													}}
												>
													{appStore.state.riskRate
														? appStore.state.riskRate
														: "Normal"}
												</Text>
											</View>
										</View>
									</View>
								</View>
								<View style={[styles.container]}>
									<View style={{ justifyContent: "center" }}>
										<Text style={[styles.dashText]}>Current</Text>
										<Text style={{ fontSize: 20, letterSpacing: 1 }}>
											Cardiovascular
										</Text>
										<Text style={[styles.dashText]}>Risk %</Text>
									</View>
									<View>
										<View style={[styles.circleContent]}>
											<View
												style={{ alignSelf: "center", alignItem: "center" }}
											>
												<Text style={{ fontSize: 40, paddingVertical: 40 }}>
													{appStore.state.percentageRisk
														? appStore.state.percentageRisk
														: 0}
													%
												</Text>
											</View>
										</View>
									</View>
								</View>
								<View style={{ paddingVertical: 15 }}>
									<View>
										<Text style={{ marginLeft: 10, fontSize: 14 }}>
											Daily Healthy Tips
										</Text>
										<View style={[styles.healthContainer]} />
									</View>
									{
                    !this.state.networkError ? (
                      <FlatList
                        data={this.state.healthTips}
                        keyExtractor={(i, index) => index.toString()}
                        renderItem={(data) => <TipsRow {...data.item}/>}
                        ListEmptyComponent={
                          <View>
                            <Text>No available health tips to show here!</Text>
                          </View>
                        }
                      />
                    ) : (
                      <View>
                        <Text>No available health tips to show here!</Text>
                      </View>
                    )
                  }
								</View>
							</View>
						)}
					</Subscribe>
				</Provider>
			</ScrollView>
		);
	}
}

const TipsRow = props => {
	return (
		<View style={{ flexDirection: "row", paddingVertical: 5 }}>
			<Icon
				name="check-square-o"
				style={{
					alignSelf: "center",
					fontSize: 14,
					color: "green",
					paddingRight: 5
				}}
			/>
			<View>
				<Text style={[styles.healthItem]}>{props.Description}</Text>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingVertical: 10
	},
	dashText: {
		fontSize: 30,
		letterSpacing: 1
	},
	circleContent: {
		backgroundColor: "#eee",
		width: 140,
		height: 140,
		borderRadius: 140 / 2
	},
	healthContainer: {
		borderColor: "#000",
		borderWidth: 1,
		width: "100%",
		marginVertical: 10
	},
	healthList: {
		width: 40,
		height: 40,
		// borderRadius: 40 / 2,
		// backgroundColor: "#eee",
		justifyContent: "center"
	},
	healthItem: {
		flex: 1,
		flexWrap: "wrap",
		marginLeft: 10,
		alignContent: "center"
	}
});
export default Dashboard;
