import Welcome from './Welcome';
import Login from './Login';
import Register from './Register';
import Guest from './Guest';
import Result from './Result';
import Dashboard from './Dashboard';
import HealthTips from './HealthTips';
import History from './History';
import Settings from './Settings';
import Loading from './Loading';
import GuestForm2 from './GuestForm2';
import GuestForm3 from './GuestForm3';
import GuestForm4 from './GuestForm4';

export {
	Welcome,
	Login,
	Register,
	Guest,
	Result,
	Dashboard,
	HealthTips,
  History,
  Settings,
  Loading,
  GuestForm2,
  GuestForm3,
  GuestForm4,
};
