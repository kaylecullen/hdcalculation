import React, { PureComponent } from "react";
import { View, Image, Text, Dimensions } from "react-native";
import { CustomButton } from "../components";
import { strings } from "../utils/strings";
class Welcome extends PureComponent {
	static navigationOptions = ({ navigation }) => ({
		header: null
	});

	render() {
		const { height, width } = Dimensions.get("window");
		return (
			<View style={{ flex: 1, backgroundColor: "#fff" }}>
				{/* <Image
					source={require("../assets/background-landingpage.png")}
					resizeMode="stretch"
					style={{ width: width, height: height }}
				/> */}
				<View
					style={{
						paddingHorizontal: 20,
						position: "absolute",
						width: width,
						paddingVertical: 50
					}}
				>
					<View style={{ paddingVertical: 50 }}>
						<Image
							source={require("../assets/ActualLogo.png")}
							resizeMode="stretch"
							style={{ width: "100%", height: "85%" }}
						/>
					</View>
					<View>
						<View style={{ paddingTop: 70}}>
              <CustomButton
                label={strings.login}
                onPress={() => this.props.navigation.navigate("Login")}
                full
              />
              <CustomButton
                label={strings.register}
                onPress={() => this.props.navigation.navigate("Register")}
                full
              />
            </View>
						
						{/* <View>
							<Text
								style={{
									textAlign: "center",
									paddingVertical: 10,
									color: "#000"
								}}
								onPress={() => this.props.navigation.navigate("Register")}
							>
								{strings.create_account}
							</Text>
						</View> */}
					</View>
				</View>
			</View>
		);
	}
}

export default Welcome;
