import React, { PureComponent } from "react";
import {
	View,
	Dimensions,
	ScrollView,
	TouchableOpacity,
	AsyncStorage
} from "react-native";
import { strings } from "../utils/strings";
import LinearGradient from "react-native-linear-gradient";
import { Form, Radio, Left, Button, Picker } from "native-base";
import { FormInput, CustomButton, RadioButton } from "../components";
import Icon from "react-native-vector-icons/FontAwesome";
import { theme } from "../../theme";
import { Text, RadioButton as RB, HelperText } from "react-native-paper";
import {
	TOKEN,
	getStorageValue,
	UNAME,
	PERCENTAGE_RISK,
	RISK_LEVEL,
	setStorageValue
} from "../utils/auth";
import { appContainer, AppContainer } from "../container";
import { fetchUrl } from "../utils/fetch";
import { endpoint } from "../utils/endpoints";
import NumericInput from "react-native-numeric-input";
import { Provider, Subscribe } from "unstated";
import SFNumberPicker from "react-native-sf-numberpicker";

let initialState = {
	gender: null,
	smoker: null,
	diabetes: null,
	age: null,
	exercise_daily: null
};
class Guest extends PureComponent {
	constructor() {
		super();

		this.state = {
			user: initialState,
			errors: [],
			isLogged: false
		};

		let age = null;
	}

	componentWillUnmount() {
		this.setState({ user: initialState });
  }

	onGenderSelected = async value => {
		if (value !== "placeholder") {
			await this.setState({ user: { ...this.state.user, gender: value } });
		}
	};

	onSubmitForm = async () => {
    let data = { ...this.state.user };
    this.setState({ errors: [] });

		const pushErrors = await Object.entries(data).map(val => {
			if (!val[1]) {
				this.setState(prevState => ({
					errors: [...prevState.errors, val[0]]
				}));
			}
		});

		if (this.state.errors.length <= 0) {
			await appContainer.setState({
				calculatorValues: {
					...appContainer.state.calculatorValues,
					...this.state.user,
				}
      }, () => {
        this.props.navigation.navigate('GuestForm2');
      });
		}
	};

	render() {
		const { height, width } = Dimensions.get("window");
		return (
			<ScrollView>
				<Provider inject={[appContainer]}>
					<Subscribe to={[AppContainer]}>
						{appStore => (
							<View style={{ marginHorizontal: 20 }}>
								<Form>
									<View style={{ paddingVertical: 15 }}>
										<Text style={{ fontSize: 16, fontWeight: "bold" }}>
											Personal Information
										</Text>
									</View>
									<View style={{ paddingVertical: 5 }}>
										<Text style={{ fontSize: 16 }}>Age:</Text>
										<View
											style={{ paddingHorizontal: 20, paddingVertical: 10 }}
										>
                      <SFNumberPicker 
                        width={200} 
                        height={50} 
                        maxNumber={120} 
                        onNumberChanged={ (tag, value) => this.setState({ user: { ...this.state.user, age: value}})} 
                        fontSize={16}
                      />
										</View>
										<HelperText
											type="error"
											visible={this.state.errors.includes("age")}
											style={{ color: theme.colors.secondary }}
										>
											Please input age!
										</HelperText>
									</View>
									<View style={{ paddingVertical: 5 }}>
										<Text style={{ fontSize: 16 }}>Gender:</Text>
										<View
											style={{ paddingHorizontal: 20, paddingVertical: 10 }}
										>
											<View style={{ borderColor: "#ccc", borderWidth: 1 }}>
												<Picker
													mode="dropdown"
													note={false}
													selectedValue={this.state.user.gender}
													onValueChange={value => this.onGenderSelected(value)}
												>
													<Picker.Item
														label="Pick Gender"
														value="placeholder"
													/>
													<Picker.Item label="Male" value="Male" />
													<Picker.Item label="Female" value="Female" />
												</Picker>
											</View>
										</View>
                    <HelperText
                        type="error"
                        visible={this.state.errors.includes("gender")}
                        style={{ color: theme.colors.secondary }}
                      >
                        Please select gender!
                      </HelperText>
									</View>
									<View style={{ paddingVertical: 15 }}>
										<Text style={{ fontSize: 16, fontWeight: "bold" }}>
											Additional Information
										</Text>
									</View>
									<View>
										<RadioButton
											data={["Yes", "No"]}
											label={"Are you a smoker?"}
											selected={this.state.user.smoker}
											onValueChange={selected =>
												this.setState({
													user: { ...this.state.user, smoker: selected }
												})
											}
											errorVisible={this.state.errors.includes("smoker")}
											errorMessage={strings.empty_field}
										/>
									</View>
									<View>
										<RadioButton
											data={["Yes", "No"]}
											label={"Has diabetes?"}
											selected={this.state.user.diabetes}
											onValueChange={selected =>
												this.setState({
													user: { ...this.state.user, diabetes: selected }
												})
											}
											errorVisible={this.state.errors.includes("diabetes")}
											errorMessage={strings.empty_field}
										/>
									</View>
									<View>
										<RadioButton
											data={["Yes", "No"]}
											label={"Do you exercise regularly?"}
											selected={this.state.user.exercise_daily}
											onValueChange={selected =>
												this.setState({
													user: { ...this.state.user, exercise_daily: selected }
												})
											}
											errorVisible={this.state.errors.includes(
												"exercise_daily"
											)}
											errorMessage={strings.empty_field}
										/>
									</View>
								</Form>
								<View style={{ paddingBottom: 15 }}>
									<CustomButton
										label={"Next"}
										onPress={() => this.onSubmitForm()}
									/>
								</View>
							</View>
						)}
					</Subscribe>
				</Provider>
			</ScrollView>
		);
	}
}

export default Guest;
