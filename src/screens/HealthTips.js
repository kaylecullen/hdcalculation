import React, { PureComponent } from "react";
import { View, Text, TouchableOpacity, ScrollView } from "react-native";
import { Left, Item, Input } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { strings } from "../utils/strings";
import { endpoint } from "../utils/endpoints";
import { fetchUrl } from "../utils/fetch";
import {
	List,
	ActivityIndicator,
	Portal,
	Dialog,
	Button
} from "react-native-paper";
import { theme } from "../../theme";

class HealthTips extends PureComponent {
	state = {
		healthTips: null,
		selectedTip: null,
    showDialog: false, 
    networkError: false,
	};

	static navigationOptions = ({ navigation }) => ({
		title: strings.health_tips,
		headerStyle: {
			backgroundColor: theme.colors.navigation
		},
		headerTintColor: theme.colors.surface,
		headerLeft: (
			<Left>
				<TouchableOpacity onPress={() => navigation.openDrawer()}>
					<Icon
						name="bars"
						style={{ color: "#fff", fontSize: 20, paddingLeft: 15 }}
					/>
				</TouchableOpacity>
			</Left>
		)
	});

	componentDidMount = async () => {
    await this.loadData();
  };
  
  loadData = async() => {
    const healthTips = await fetchUrl(endpoint.healthTips, "GET");
    if(healthTips === 'error') {
      this.setState({ networkError: true}); 
    }
		else{
			this.setState({ healthTips: healthTips, networkError: false });
		}
  }

  tapToReload = async() => {
    await this.setState({ networkError: false });
    await this.loadData();
  }

	selectTips = val => {
		this.setState({ selectedTip: val, showDialog: true });
	};

	render() {
		if(!this.state.networkError){
      return (
        <View style={{ flex: 1 }}>
          {this.state.healthTips ? (
            <ScrollView>
              <View style={{ paddingHorizontal: 20, paddingVertical: 30 }}>
                <Text style={{ fontSize: 35 }}>{strings.health_tips}</Text>
                <View>
                  {this.state.healthTips.map((val, key) => {
                    return (
                      <List.Item
                        title={val.Title}
                        description={val.Description}
                        key={key}
                        style={{ borderBottomColor: "#ccc", borderBottomWidth: 1 }}
                        onPress={() => this.selectTips(val)}
                        left={props => (
                          <Icon
                            name="check-square-o"
                            style={{
                              alignSelf: "center",
                              fontSize: 14,
                              color: "green",
                              paddingRight: 5
                            }}
                          />
                        )}
                      />
                    );
                  })}
                </View>
  
                <Portal>
                  {this.state.selectedTip ? (
                    <Dialog
                      visible={this.state.showDialog}
                      onDismiss={() =>
                        this.setState({ selectedTip: null, showDialog: false })
                      }
                    >
                      <Dialog.Title>
                        <Text>{this.state.selectedTip.Title}</Text>
                      </Dialog.Title>
                      <Dialog.Content>
                        <Text style={{ fontWeight: "bold" }}>{strings.description+ ': '} </Text>
                        <View style={{ padding: 10 }}>
                          <Text>{this.state.selectedTip.Description}</Text>
                        </View>
                      </Dialog.Content>
                      <Dialog.Actions>
                        <Button
                          onPress={() =>
                            this.setState({ selectedTip: null, showDialog: false })
                          }
                          color={theme.colors.primary}
                        >
                          {strings.close.toUpperCase()}
                        </Button>
                      </Dialog.Actions>
                    </Dialog>
                  ) : null}
                </Portal>
              </View>
            </ScrollView>
          ) : (
            <View style={{ flex: 1 }}>
              <ActivityIndicator
                size="large"
                color={theme.colors.primary}
                style={{ flex: 1, alignItems: "center" }}
              />
            </View>
          )}
        </View>
      );
    }else{
      return(
        <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center'}}>
          <Button mode="text" onPress={ () => this.tapToReload() }>
            Tap to reload
          </Button>
        </View>
      )
    }
	}
}

export default HealthTips;
