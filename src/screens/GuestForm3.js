import React, { PureComponent } from "react";
import { View, ScrollView, TouchableOpacity } from "react-native";
import { Text, RadioButton, Caption, HelperText } from "react-native-paper";
import { Left } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { appContainer } from "../container";
import { strings } from "../utils/strings";
import { theme } from "../../theme";
import NumericInput from "react-native-numeric-input";
import { RadioButton as Radio, CustomButton, FormInput } from "../components";
import SFNumberPicker from "react-native-sf-numberpicker";

initialState = {
	ldl: null,
	hdl: null
};

class GuestForm3 extends PureComponent {
	constructor() {
		super();

		this.state = {
			user: initialState,
			errors: []
		};
  }
  
  componentWillUnmount(){
    this.setState({ user: initialState });
  }

	onHdlPicker = value => {
		this.setState({ user: { ...this.state.user, hdl: value } });
	};

	onLdlPicker = value => {
		this.setState({ user: { ...this.state.user, ldl: value } });
	};

	onFormSubmit = async () => {
		let data = { ldl: this.state.user.ldl, hdl: this.state.user.hdl };

		this.setState({ errors: [] });

		const pushErrors = await Object.entries(data).map(val => {
			if (!val[1]) {
				this.setState(prevState => ({
					errors: [...prevState.errors, val[0]]
				}));
			}
		});

		if (this.state.errors.length <= 0) {
			await appContainer.setState(
				{
					calculatorValues: {
						...appContainer.state.calculatorValues,
						...data
					}
				},
				() => {
					this.props.navigation.navigate("GuestForm4");
				}
			);
		}
	};

	render() {
		return (
			<ScrollView>
				<View style={{ marginHorizontal: 20 }}>
					<View>
						<View style={{ paddingVertical: 15 }}>
							<Text style={{ fontSize: 16, fontWeight: "bold" }}>
								Cholesterol
							</Text>
						</View>
						<View>
							{/* <Text style={{ fontSize: 16 }}>
								Low Density Lipoprotein Level
							</Text> */}
							<View style={{ paddingHorizontal: 20 }}>
								{/* <SFNumberPicker
									width={200}
									height={50}
									maxNumber={250}
									onNumberChanged={(tag, value) =>
										this.setState({ user: { ...this.state.user, ldl: value } })
									}
									fontSize={16}
                /> */}
                <FormInput
                  label={"Low Density Lipoprotein Level"}
                  onValueChange={value =>
                    this.setState({
                      user: { ...this.state.user, ldl: value }
                    })
                  }
                  value={this.state.user.ldl}
                  isNumber={true}
                  hasUnit={"mg/dl"}
                />
							</View>
							<Text>OR</Text>
							<View style={{ paddingTop: 5 }}>
								<RadioButton.Group
									onValueChange={value => this.onLdlPicker(value)}
									value={this.state.user.ldl}
								>
									<View
										style={{
											flexDirection: "row",
											justifyContent: "space-between",
											alignSelf: "flex-start",
											paddingHorizontal: 20
										}}
									>
										<RButton title="Low" value={"110"} />
										<RButton title="Normal" value={"125"} />
									</View>
									<View
										style={{
											flexDirection: "row",
											justifyContent: "space-between",
											alignSelf: "flex-start",
											paddingHorizontal: 20
										}}
									>
										<RButton title="High" value={"155"} />
										<RButton title="I don't know" value={"120"} />
									</View>
								</RadioButton.Group>
							</View>
						</View>
						<HelperText
							type="error"
							visible={this.state.errors.includes("ldl")}
							style={{ color: theme.colors.secondary }}
						>
							Field is required!
						</HelperText>
					</View>
					<View>
						{/* <Text style={{ fontSize: 16 }}>High Density Lipoprotein Level</Text> */}
						<View style={{ paddingHorizontal: 20 }}>
              {/* <SFNumberPicker 
                width={200} 
                height={50} 
                maxNumber={200} 
                onNumberChanged={ (tag, value) => this.setState({ user: { ...this.state.user, hdl: value}})} 
                fontSize={16}
              /> */}
              <FormInput
                label={"High Density Lipoprotein Level"}
                onValueChange={value =>
                  this.setState({
                    user: { ...this.state.user, hdl: value }
                  })
                }
                value={this.state.user.hdl}
                isNumber={true}
                hasUnit={"mg/dl"}
              />
						</View>
						<Text>OR</Text>
						<View style={{ paddingTop: 5 }}>
							<RadioButton.Group
								onValueChange={value => this.onHdlPicker(value)}
								value={this.state.user.hdl}
							>
								<View
									style={{
										flexDirection: "row",
										justifyContent: "space-between",
										alignSelf: "flex-start",
										paddingHorizontal: 20
									}}
								>
									<RButton title="Low" value={"35"} />
									<RButton title="Normal" value={"45"} />
								</View>
								<View
									style={{
										flexDirection: "row",
										justifyContent: "space-between",
										alignSelf: "flex-start",
										paddingHorizontal: 20
									}}
								>
									<RButton title="High" value={"60"} />
									<RButton title="Don't know" value={"100"} />
								</View>
							</RadioButton.Group>
						</View>
						<HelperText
							type="error"
							visible={this.state.errors.includes("hdl")}
							style={{ color: theme.colors.secondary }}
						>
							Field is required!
						</HelperText>
					</View>
					<View>
						<Caption>
							Enter your cholesterol numbers in the boxes, or if your doctor
							told you your cholesterol is low, normal, or high, then choose
							that. If you don’t know your numbers or level, choose Don’t Know,
							but the result won’t be personal to you.
						</Caption>
					</View>
					<View
						style={{
							paddingVertical: 20,
							flexDirection: "row",
							justifyContent: "center"
						}}
					>
						<CustomButton
							label={"Previous"}
							onPress={() => this.props.navigation.navigate("GuestForm2")}
						/>
						<CustomButton
							label={"Next"}
							onPress={() => this.onFormSubmit()}
							// onPress={() => this.props.navigation.navigate('GuestForm4')}
						/>
					</View>
				</View>
			</ScrollView>
		);
	}
}

const RButton = props => (
	<View style={{ flexDirection: "row", paddingHorizontal: 30 }}>
		<RadioButton
			value={props.value}
			color={theme.colors.text}
			uncheckedColor={"#ccc"}
		/>
		<Text style={{ alignSelf: "center", fontSize: 16 }}>{props.title}</Text>
	</View>
);

export default GuestForm3;
