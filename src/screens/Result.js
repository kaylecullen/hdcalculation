import React, { PureComponent } from "react";
import { View, Text, StyleSheet } from "react-native";
import { strings } from "../utils/strings";
import LinearGradient from "react-native-linear-gradient";
import { CustomButton } from "../components";
import { getStorageValue, TOKEN } from "../utils/auth";
import { Provider, Subscribe } from 'unstated';
import { Portal, Dialog, Button } from 'react-native-paper';
import { appContainer, AppContainer } from '../container';
import { theme } from "../../theme";
import { StackActions, NavigationActions } from 'react-navigation';

class Result extends PureComponent {
	constructor() {
		super();
	}

	state = {
    hasToken: null,
    age: null,
	};

	async componentDidMount() {
		const token = await getStorageValue(TOKEN).then(res => {
			return res;
		});

    this.setState({ hasToken: token, age: await appContainer.state.age });
	}

	static navigationOptions = ({ navigation }) => ({
		title: strings.result,
		headerStyle: {
			backgroundColor: theme.colors.navigation
		},
		headerTintColor: theme.colors.surface
	});

	calculateAgain = () => {
		this.props.navigation.goBack();
	};

	render() {
    const level = this.props.navigation.state.params.riskRate;
    const result = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Guest'})]
    });
    const dashboard = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Dashboard'})]
    });
    const highDef = "Research has shown that these factors significantly increase the risk of heart and blood vessel(cardiovascular) disease.";
    const average = "Some major risk factors can be modified, treated or controlled through medications or lifestyle change.";
    const lowDef = "These factors are associated with increased risk of cardiovascular disease, but their significance and prevalence haven't yet determined.";
		return (
			// <LinearGradient
			// 	style={{ flex: 1 }}
			// 	start={{ x: 0.75, y: 0 }}
			// 	end={{ x: 1, y: 0.75 }}
			// 	colors={["#0e3563", "#0e3563", "#051e41"]}
			// >
				<Provider inject={[ appContainer ]}>
          <Subscribe to={[ AppContainer ]}>
            {(appStore) => (
              <View style={{ margin: 20 }}>
              <View
                style={[ styles.container ]}
              >
                <View style={{ paddingTop: 20, paddingHorizontal: 20 }}>
                  <Text style={[ styles.textStyle ]}>{strings.result}</Text>
                </View>
                <View
                  style={[ styles.resultContent ]}
                />
                <View style={{ paddingVertical: 30, paddingHorizontal: 20 }}>
                  <Text style={[ styles.textStyle ]}>
                    {strings.cardio_risk}
                  </Text>
                  {/* dynamic text */}
                  <Text style={[ styles.bigTextStyle ]}>
                    {appStore.state.percentageRisk} %
                  </Text>
                  <Text style={[ styles.textStyle, { paddingVertical: 5 }]}>
                    Age: {this.state.age + " "}years
                    old
                  </Text>
                  <Text style={[ styles.textStyle, { paddingVertical: 5 }]}>
                    Risk Level:{" "}
                    <Text
                      style={{
                        color:
                          appStore.state.riskRate === "Low"
                            ? "green"
                            : appStore.state.riskRate === "Average"
                            ? "yellow"
                            : "red",
                        fontWeight: "bold"
                      }}
                    >
                      {" "}
                      {appStore.state.riskRate}
                    </Text>
                  </Text>
                  <Text style={[ styles.textStyle ]}>
                    <Text style={{ fontSize: 14}}>{" " +
                      appStore.state.riskRate === "Low" ?
                      lowDef : appStore.state.riskRate === "Average" ?
                      average : highDef
                    }</Text>
                  </Text>
                </View>
              </View>
              <View style={[ styles.buttonStyles ]}>
                <CustomButton
                  label={strings.calculate_again}
                  full
                  onPress={() => this.props.navigation.dispatch(result)}
                />
              </View>
              <View style={{ justifyContent: "flex-end" }}>
                {this.state.hasToken ? (
                  <CustomButton
                    label={strings.go_to_dashboard}
                    full
                    onPress={ () => 
                      this.props.navigation.dispatch(dashboard)}
                  />
                ) : null}
              </View>
            </View>
            )}
          </Subscribe>
        </Provider>
			// </LinearGradient>
		);
	}
}


const styles = StyleSheet.create(
  {
    textStyle: {
      color: theme.colors.text, 
      fontSize: 16,
    },
    buttonStyles: {
      alignSelf: "center", 
      paddingVertical: 20
    },
    bigTextStyle: {
      fontSize: 60,
      color: theme.colors.text,
    },
    resultContent: {
      borderColor: theme.colors.text,
      borderWidth: 2,
      width: "95%",
      alignSelf: "center",
      marginTop: 10
    },
    container: {
      borderColor: theme.colors.text, 
      borderWidth: 2, 
      borderRadius: 8 
    }
  }
)

export default Result;
