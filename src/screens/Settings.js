import React, { PureComponent } from "react";
import {
	View,
	TouchableOpacity,
	ActivityIndicator,
	ScrollView
} from "react-native";
import { Text, HelperText, Portal, Snackbar } from "react-native-paper";
import { strings } from "../utils/strings";
import { theme } from "../../theme";
import { Left, Form, Button } from "native-base";
import { FormInput, CustomButton } from "../components";
import Icon from "react-native-vector-icons/FontAwesome";
import { Provider, Subscribe } from "unstated";
import { appContainer, AppContainer, authContainer } from "../container";
import { onLogout } from '../container/AuthContainer';

const initialState = {
	user: {
		password: null,
		current: null,
		retype: null
	},
	showPassword: false,
	showCurrent: false,
	showRetype: false,
	errors: [],
  isVisible: false,
  networkError: false,
};
class History extends PureComponent {
	constructor() {
		super();
	}

	state = initialState;

	static navigationOptions = ({ navigation }) => ({
		title: strings.settings,
		headerStyle: {
			backgroundColor: theme.colors.navigation
		},
		headerTintColor: theme.colors.surface,
		headerLeft: (
			<Left>
				<TouchableOpacity onPress={() => navigation.openDrawer()}>
					<Icon
						name="bars"
						style={{ color: "#fff", fontSize: 20, paddingLeft: 15 }}
					/>
				</TouchableOpacity>
			</Left>
		)
  });
  
  handleOnLogout = async() => {
    await onLogout();
    this.props.navigation.navigate('Auth');
  }

	onSubmit = async () => {
		// console.log(this.state);
		this.setState({ errors: [] });

		await Object.entries(this.state.user).map(val => {
			if (!val[1]) {
				this.setState(prevState => ({
					errors: [...prevState.errors, val[0]]
				}));
			}
		});

		if (this.state.errors.length <= 0) {
			const isUpdated = await authContainer.changePassword(this.state.user);

			if (isUpdated === true) {
        await setTimeout(() => this.changeSuccess(), 3000);
      } else {
        this.setState({ errors: isUpdated });
			}
		}
	};

	changeSuccess = async() => {
    await this.setState({ user: initialState.user, isVisible: true });
    
	};

	render() {
		return (
			<ScrollView style={{ flex: 1 }}>
				<Provider inject={[appContainer]}>
					<Subscribe to={[AppContainer]}>
						{appStore => (
							<View>
								<View style={{ paddingHorizontal: 20, paddingVertical: 30 }}>
									<Text style={{ fontSize: 20 }}>Change Password</Text>
								</View>
								<View style={{ paddingHorizontal: 20 }}>
									<Form>
										<FormInput
											label={"Current Password"}
											onValueChange={value =>
												this.setState({
													user: { ...this.state.user, current: value }
												})
											}
											value={this.state.user.current}
											isPrimary={true}
											isPassword={true}
											showPassword={() =>
												this.setState({
													showCurrent: !this.state.showCurrent
												})
											}
											isShown={this.state.showCurrent}
											errorVisible={
												this.state.errors.includes("current") ||
												this.state.errors.includes("current_invalid")
											}
											errorMessage={
												this.state.errors.includes("current_invalid")
													? strings.password_incorrect
													: strings.password_error
											}
										/>
										<FormInput
											label={strings.password_new}
											onValueChange={value =>
												this.setState({
													user: { ...this.state.user, password: value }
												})
											}
											value={this.state.user.password}
											isPassword={true}
											showPassword={() =>
												this.setState({
													showPassword: !this.state.showPassword
												})
											}
											isPrimary={true}
											isShown={this.state.showPassword}
											errorVisible={
												this.state.errors.includes("password") ||
												this.state.errors.includes("password_invalid")
											}
											errorMessage={
												this.state.errors.includes("password_invalid")
													? strings.password_invalid
													: strings.password_error
											}
										/>
										<FormInput
											label={strings.retype_password}
											onValueChange={value =>
												this.setState({
													user: { ...this.state.user, retype: value }
												})
											}
											value={this.state.user.retype}
											isPassword={true}
											showPassword={() =>
												this.setState({
													showRetype: !this.state.showRetype
												})
											}
											isPrimary={true}
											isShown={this.state.showRetype}
											errorVisible={
												this.state.errors.includes("retype") ||
												this.state.errors.includes("confirm_invalid")
											}
											errorMessage={
												this.state.errors.includes("confirm_invalid")
													? strings.confirm_password_invalid
													: strings.password_error
											}
										/>
										{/* <CustomButton
											full
											bordered
											style={{ color: theme.colors.primary, borderRadius: 50 }}
                      onPress={() => this.onSubmit()}
                      disabled={appStore.state.disablePassword}
										> */}
											{/* <Text
												style={{
													color: theme.colors.primary,
													fontWeight: "bold"
												}}
											>
												{strings.submit.toUpperCase()}
											</Text>
                    </Button> */}
                    <CustomButton
                      label={strings.submit.toUpperCase()}
                      full
                      onPress={() => this.onSubmit()}
                      disabled={appStore.state.disablePassword}
                    />
									</Form>
								</View>
							</View>
						)}
					</Subscribe>
				</Provider>

				<Portal>
					<Snackbar
						visible={this.state.isVisible}
						onDismiss={() => this.setState({ isVisible: false })}
						duration={3000}
					>
						{strings.changes_saved}
					</Snackbar>
				</Portal>
			</ScrollView>
		);
	}
}

export default History;
