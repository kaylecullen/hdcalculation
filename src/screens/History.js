import React, { PureComponent } from "react";
import {
	View,
	Text,
	TouchableOpacity,
  FlatList,
  ScrollView,
} from "react-native";
import { Left } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { strings } from "../utils/strings";
import { getStorageValue, UNAME } from "../utils/auth";
import moment from "moment-timezone";
import { theme } from "../../theme";
import { appContainer } from "../container";
import { Provider, Subscribe } from 'unstated';
import { ActivityIndicator, Button } from 'react-native-paper';

class HealthTips extends PureComponent {
	state = {
		chosenDate: new Date(),
		username: null,
		historyDetails: null,
		newHistory: [],
    pastHistory: [],
    networkError: false,
	};

  componentDidMount(){
    this.setState({ newHistory: [], pastHistory: [], historyDetails: null });
    this.refreshList();
  }

  refreshList = async () => {
    const uname = await getStorageValue(UNAME).then(res => {
			return res;
		});

		if (uname) {
      const history = await appContainer.getHistory(uname);
      if(history === 'error'){
        console.log(history);
        await this.setState({ networkError: true });
      }else{
        this.setState({ historyDetails: history, networkError: false });
      }
		}

		await this.state.historyDetails.filter(data => {
			let date = moment(data.CreatedDate).format("M/D/YYYY");
			if (date === new Date().toLocaleDateString()) {
				this.setState({ newHistory: [data, ...this.state.newHistory] });
			} else {
				this.setState({ pastHistory: [data, ...this.state.pastHistory] });
			}
		});
  }

  tapToReload = async() => {
    await this.setState({ networkError: false });
    await this.refreshList();
  }

	static navigationOptions = ({ navigation }) => ({
		title: strings.history,
		headerStyle: {
			backgroundColor: theme.colors.navigation
		},
		headerTintColor: theme.colors.surface,
		headerLeft: (
			<Left>
				<TouchableOpacity onPress={() => navigation.openDrawer()}>
					<Icon
						name="bars"
						style={{ color: "#fff", fontSize: 20, paddingLeft: 15 }}
					/>
				</TouchableOpacity>
			</Left>
		)
	});

	render() {
		if(!this.state.networkError){
      return (
        <View style={{ flex: 1 }}>
          {this.state.historyDetails ? (
            <ScrollView>
              <View style={{ paddingHorizontal: 20, paddingVertical: 30 }}>
                <Text style={{ fontSize: 35 }}>{strings.history}</Text>
                {this.state.historyDetails.length > 0 ? (
                  <View>
                    <FlatList
                      data={this.state.historyDetails}
                      keyExtractor={(i, index) => index.toString()}
                      renderItem={(data) => <DataRow {...data.item}/>}
                    />
                  </View>
                  // <View style={{ paddingVertical: 10 }}>
                  //   <View>
                  //     {
                  //       this.state.newHistory.length > 0 ? (
                  //         <View>
                  //           <Text style={{ fontSize: 15 }}>Today</Text>
                  //           <FlatList
                  //             data={this.state.newHistory}
                  //             keyExtractor={(i, index) => index.toString()}
                  //             renderItem={(data) => <DataRow {...data.item}/>}
                  //           />
                  //         </View>
                  //       ) : null
                  //     }
                  //     <View>
                  //       <Text style={{ fontSize: 15 }}>Older</Text>
                  //       <FlatList
                  //         data={this.state.pastHistory}
                  //         keyExtractor={(i, index) => index.toString()}
                  //         renderItem={(data) => <DataRow {...data.item}/>}
                  //       />
                  //     </View>
                  //   </View>
                  // </View>
                ) : (
                  <View style={{ padding: 15 }}>
                    <Text style={{ fontWeight: "bold", fontSize: 15 }}>
                      {strings.no_saved_history}
                    </Text>
                  </View>
                )}
              </View>
            </ScrollView>
          ) : (
            <View style={{ flex: 1 }}>
              <ActivityIndicator
                size="large"
                color={theme.colors.primary}
                style={{ flex: 1, alignItems: "center" }}
              />
            </View>
          )}
        </View>
      );
    }else{
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center'}}>
          <Button mode="text" onPress={ () => this.tapToReload() }>
            Tap to reload
          </Button>
        </View>
      )
    }
	}
}

const DataRow = props => {
  return (
    <View
		style={{
			flexDirection: "row",
			justifyContent: "space-between",
			paddingVertical: 15
		}}
	>
		<View style={{ alignItems: "center" }}>
			<View
				style={{ 
          padding: 15, 
          backgroundColor: props.RiskLevel === 'Low' ? theme.colors.low : props.RiskLevel === 'Average' ? theme.colors.average : theme.colors.high, 
          borderRadius: 50 
      }}
			/>
		</View>
		<View style={{ alignItems: "center" }}>
			<Text>PERCENTAGE</Text>
			<Text style={{ fontSize: 15 }}>{props.CardiovascularRiskPerc}</Text>
		</View>
		<View style={{ alignItems: "center" }}>
			<Text>RISK LEVEL</Text>
			<Text style={{ fontSize: 15 }}>{props.RiskLevel}</Text>
		</View>
		<View style={{ alignItems: "center" }}>
			<Text>AGE</Text>
			<Text style={{ fontSize: 15 }}> {props.HeartAge}</Text>
		</View>
		<View style={{ alignItems: "center" }}>
      <Text>DATE</Text>
      <Text>{moment(props.CreatedDate).format('M/D/YYYY')}</Text>
    </View>
	</View>
  )
}

export default HealthTips;
