import React, { PureComponent } from "react";
import { View, ScrollView, TouchableOpacity } from "react-native";
import { Text, RadioButton, HelperText } from "react-native-paper";
import { Left } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { appContainer, AppContainer } from "../container";
import { strings } from "../utils/strings";
import { theme } from "../../theme";
import NumericInput from "react-native-numeric-input";
import { RadioButton as Radio, CustomButton } from "../components";
import { Provider, Subscribe } from "unstated";

initialState = {
	eating_habits: null,
	alcohol: null,
	house_smoke: null,
	other_activity: null
};

class GuestForm4 extends PureComponent {
	constructor() {
		super();

		this.state = {
			user: initialState,
			errors: []
		};
	}

	componentWillUnmount() {
		this.setState({ user: initialState });
	}

	onFormSubmit = async () => {
		this.setState({ errors: [] });

		const pushErrors = await Object.entries(this.state.user).map(val => {
			if (!val[1]) {
				this.setState(prevState => ({
					errors: [...prevState.errors, val[0]]
				}));
			}
		});

		if (this.state.errors.length <= 0) {
			await appContainer.setState({
				calculatorValues: {
					...appContainer.state.calculatorValues,
					...this.state.user
				},
				isLoading: true
			});

			const result = await appContainer.calculateForm();

			if (result) {
				const jsonData = JSON.parse(result);
				console.log(jsonData);
        this.props.navigation.navigate("Result", { jsonData });
        await appContainer.setState({ disableButton: false });
			}
		}
	};

	otherActivity = value => {
		this.setState({ user: { ...this.state.user, other_activity: value } });
	};

	render() {
		return (
			<ScrollView>
				<Provider inject={[ appContainer ]}>
					<Subscribe to={[ AppContainer ]}>
						{appStore => (
							<View style={{ marginHorizontal: 20 }}>
								<View style={{ paddingVertical: 15 }}>
									<Text style={{ fontSize: 16, fontWeight: "bold" }}>
										Extra Information
									</Text>
								</View>
								<View style={{ paddingVertical: 5 }}>
									<Radio
										data={["Yes", "No", "Don't know"]}
										label={
											"Are you changing your eating habits to help lower or control your blood pressure?"
										}
										selected={this.state.user.eating_habits}
										onValueChange={selected =>
											this.setState({
												user: { ...this.state.user, eating_habits: selected }
											})
										}
										errorVisible={this.state.errors.includes("eating_habits")}
										errorMessage={strings.empty_field}
									/>
								</View>
								<View style={{ paddingVertical: 5 }}>
									<Radio
										data={["Yes", "No", "Don't know"]}
										label={"Do you drink alcohol?"}
										selected={this.state.user.alcohol}
										onValueChange={selected =>
											this.setState({
												user: { ...this.state.user, alcohol: selected }
											})
										}
										errorVisible={this.state.errors.includes("alcohol")}
										errorMessage={strings.empty_field}
									/>
								</View>
								<View style={{ paddingVertical: 5 }}>
									<Radio
										data={["Yes", "No", "Don't know"]}
										label={"Does anyone in your house smoke?"}
										selected={this.state.user.house_smoke}
										onValueChange={selected =>
											this.setState({
												user: { ...this.state.user, house_smoke: selected }
											})
										}
										errorVisible={this.state.errors.includes("house_smoke")}
										errorMessage={strings.empty_field}
									/>
								</View>
								<View>
									<Text style={{ fontSize: 16 }}>
										What kind of physical activity do you currently do?
									</Text>
									<RadioButton.Group
										onValueChange={value => this.otherActivity(value)}
										value={this.state.user.other_activity}
									>
										<View>
											<RButton
												title="Aerobic Workout"
												value={"aerobic_workout"}
											/>
											<RButton
												title="Running/Jogging"
												value={"running_jogging"}
											/>
											<RButton title="Walking" value={"walking"} />
											<RButton title="Bicycling" value={"bicycling"} />
											<RButton title="Swimming" value={"swimming"} />
											<RButton title="None" value={"none"} />
										</View>
									</RadioButton.Group>
									<HelperText
										type="error"
										visible={this.state.errors.includes("other_activity")}
										style={{ color: theme.colors.secondary }}
									>
										Field is required!
									</HelperText>
								</View>
								<View
									style={{
										paddingVertical: 20,
										flexDirection: "row",
										justifyContent: "center"
									}}
								>
									<CustomButton
										label={"Previous"}
										onPress={() => this.props.navigation.navigate("GuestForm3")}
										disabled={appStore.state.disableButton}
									/>
									<CustomButton
										label={"Submit"}
										onPress={() => this.onFormSubmit()}
										disabled={appContainer.state.disableButton}
										// onPress={() => this.setState({ user: initialState, errors: [] })}
									/>
								</View>
							</View>
						)}
					</Subscribe>
				</Provider>
			</ScrollView>
		);
	}
}

const RButton = props => (
	<View style={{ flexDirection: "row" }}>
		<RadioButton
			value={props.value}
			color={theme.colors.text}
			uncheckedColor={"#ccc"}
		/>
		<Text style={{ alignSelf: "center", fontSize: 16 }}>{props.title}</Text>
	</View>
);

export default GuestForm4;
