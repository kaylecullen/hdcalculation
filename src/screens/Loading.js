import React, { PureComponent } from "react";
import { View, TouchableOpacity } from "react-native";
import { Text, ActivityIndicator } from "react-native-paper";
import { strings } from "../utils/strings";
import { theme } from "../../theme";
import { Left } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { getStorageValue, TOKEN } from "../utils/auth";

class Loading extends PureComponent {
	constructor() {
		super();
	}

	async componentDidMount() {
		await getStorageValue(TOKEN).then(res => {
			this.props.navigation.navigate(res ? "App" : "Auth");
		});
	}

	render() {
		return (
			<View style={{ flex: 1, justifyContent: "center" }}>
				<ActivityIndicator
					animating={true}
					color={theme.colors.primary}
					size="large"
				/>
			</View>
		);
	}
}

export default Loading;
