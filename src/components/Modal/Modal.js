import React from 'react';
import { View, Text, Modal } from 'react-native';

const Dialog = (props) => {
  return(
    <View style={{ flex: 1, marginTop: 40}}>
      <Modal visible={false} animationType='slide' transparent={false}>
        <View>
          <Text>Sample modal!</Text>
        </View>
      </Modal>
    </View>
  );
}

export default Dialog;