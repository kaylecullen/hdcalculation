import React from "react";
import { View, StyleSheet } from "react-native";
import { Text } from "native-base";
import { colors } from "../../utils/styles";
import { Button } from "react-native-paper";
import { theme } from "../../../theme";

const CustomButton = props => {
	return (
		<View style={{ margin: 10 }}>
			<Button
				mode="text"
				style={[
					styles.buttonStyle,
          { paddingHorizontal: !props.full ? 30 : 0, 
            borderColor: props.disabled ? theme.colors.inactiveTint : theme.colors.secondary,
          }
				]}
				color={theme.colors.text}
        {...props}
			>
				{props.label}
			</Button>
		</View>
	);
};

const styles = StyleSheet.create({
	buttonStyle: {
		borderWidth: 1,
		borderRadius: 30,
		paddingVertical: 5
	}
});

export default CustomButton;
