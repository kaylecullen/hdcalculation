import React from "react";
import { View, Dimensions } from "react-native";
import { Radio } from "native-base";
import { RadioButton as RButton, HelperText, Text } from "react-native-paper";
import { theme } from '../../../theme';

const RadioButton = props => {
	const { width } = Dimensions.get("window");
	const length = props.data.length;
	return (
		<View>
			{props.label ? (
				<Text style={{ fontSize: 16 }}>{props.label}</Text>
			) : null}
			<RButton.Group
				onValueChange={value => props.onValueChange(value)}
				value={props.selected}
			>
				<View style={{ flexDirection: "row" }}>
					{props.data.map((value, key) => {
						return (
							<View
								key={key}
								style={{
									flexDirection: "row",
									width: width / length,
									paddingVertical: 5
								}}
							>
								<RButton value={value} color={theme.colors.text} uncheckedColor={'#ccc'} />
								<Text style={{ alignSelf: "center" }}>
									{value}
								</Text>
							</View>
						);
					})}
				</View>
			</RButton.Group>
			<HelperText
				type="error"
				visible={props.errorVisible}
				style={{ color: theme.colors.secondary }}
			>
				{props.errorMessage}
			</HelperText>
		</View>
	);
};

export default RadioButton;
