import React from "react";
import { View } from "react-native";
import { Item, Label, Input, Text, Icon } from "native-base";
import { colors } from "../../utils/styles";
import { HelperText, TextInput } from "react-native-paper";
import { theme } from "../../../theme";

const FormInput = props => {
	if(props.hasUnit){
    return (
      <View style={{ marginVertical: 5 }}>
        <View style={{ flex: 1, flexDirection: 'row'}}>
          <View style={{ width: "90%"}}>
            <Item floatingLabel>
              <Label
                style={{
                  // color: props.isPrimary ? theme.colors.primary : colors.white
                  color: theme.colors.text
                }}
              >
                {props.label}
              </Label>
              <Input
                style={{
                  // color: props.isPrimary ? theme.colors.primary : colors.white
                  color: theme.colors.text
                }}
                onChangeText={val => props.onValueChange(val)}
                value={props.value}
                secureTextEntry={props.isPassword ? !props.isShown : false}
                keyboardType={props.isNumber ? 'number-pad' : 'default'}
              />
              {props.isPassword ? (
                <Icon
                  active
                  name={"eye"}
                  onPress={() => props.showPassword()}
                  style={{
                    color: theme.colors.text
                    // color: props.isPrimary ? theme.colors.primary : colors.white
                  }}
                />
              ) : null}
            </Item>
          </View>
          {
            props.hasUnit ? (
              <View style={{ alignSelf: 'center'}}>
                <Text style={{ fontSize: 14, fontWeight: 'bold'}}>{props.hasUnit}</Text>
              </View>
            ) : null
          }
        </View>
        <HelperText
          type="error"
          visible={props.errorVisible}
          // style={{ color: props.isPrimary ? theme.colors.primary : '#fff' }}
          style={{ color: theme.colors.secondary }}
        >
          {props.errorMessage}
        </HelperText>
      </View>
    );
  }else{
    return (
      <View style={{ marginVertical: 5 }}>
        <Item floatingLabel>
          <Label
            style={{
              // color: props.isPrimary ? theme.colors.primary : colors.white
            }}
          >
            {props.label}
          </Label>
          <Input
            // style={{
            //   color: props.isPrimary ? theme.colors.primary : colors.white
            // }}
            color={theme.colors.text}
            onChangeText={val => props.onValueChange(val)}
            value={props.value}
            secureTextEntry={props.isPassword ? !props.isShown : false}
          />
          {props.isPassword ? (
            <Icon
              active
              name={"eye"}
              onPress={() => props.showPassword()}
              style={{
                color: theme.colors.text
                // color: props.isPrimary ? theme.colors.primary : colors.white
              }}
            />
          ) : null}
        </Item>
        <HelperText
          type="error"
          visible={props.errorVisible}
          style={{ color: theme.colors.secondary }}
        >
          {props.errorMessage}
        </HelperText>
      </View>
    );
  }
};

export default FormInput;
