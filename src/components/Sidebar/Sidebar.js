import React, { PureComponent } from "react";
import {
	View,
	Text,
	TouchableOpacity,
	TouchableNativeFeedback,
	Image,
	Dimensions,
	AsyncStorage
} from "react-native";
import { Container, Content } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { styles } from "./styles";
import { strings } from "../../utils/strings";
import { appContainer, AppContainer } from "../../container";
import { onLogout } from '../../container/AuthContainer';
import { TOKEN, UNAME, RISK_LEVEL, PERCENTAGE_RISK, getStorageValue } from "../../utils/auth";
import { Provider, Subscribe } from "unstated";
import { StackActions, NavigationActions } from 'react-navigation';

class Sidebar extends PureComponent {
	// onLogout = async () => {
	// 	AsyncStorage.removeItem(TOKEN);
	// 	AsyncStorage.removeItem(UNAME);
	// 	AsyncStorage.removeItem(PERCENTAGE_RISK);
	// 	AsyncStorage.removeItem(RISK_LEVEL);

	// 	this.props.navigation.navigate("Auth");
  // };

  handleOnLogout = async() => {
    await onLogout();
    this.props.navigation.navigate('Auth');
  }
  
  async componentDidMount(){
    await getStorageValue(UNAME).then(res => {
      appContainer.setState({ username: res });
    });

  }


	render() {
    const { height } = Dimensions.get("window");
    const dashboard = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Dashboard'})]
    });
		return (
			<Provider inject={[appContainer]}>
				<Subscribe to={[AppContainer]}>
					{appStore => (
						<Container>
							<Content>
								<View>
									<Image
										source={require("../../assets/background-landingpage.png")}
										resizeMode="stretch"
										style={{ height: height / 4 }}
									/>
									<View style={{ position: "absolute", paddingVertical: 40 }}>
										<View style={{ flexDirection: "row" }}>
											<Icon name="user-circle" style={styles.headerIcon} />
											<View style={{ justifyContent: "center" }}>
												<Text style={styles.titleFontStyle}>
													{appStore.state.username}
												</Text>
											</View>
										</View>
									</View>
								</View>
								<View style={{ padding: 30 }}>
									<TouchableOpacity
										onPress={() => this.props.navigation.dispatch(dashboard)}
									>
										<View style={styles.itemContent}>
											<Icon name="home" style={styles.iconStyle} />
											<Text style={styles.textStyle}>{strings.dashboard}</Text>
										</View>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() => this.props.navigation.navigate("Guest")}
									>
										<View style={styles.itemContent}>
											<Icon name="calculator" style={styles.iconStyle} />
											<Text style={styles.textStyle}>
												{strings.hd_calculator}
											</Text>
										</View>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() => this.props.navigation.navigate("HealthTips")}
									>
										<View style={styles.itemContent}>
											<Icon name="lightbulb-o" style={styles.iconStyle} />
											<Text style={styles.textStyle}>
												{strings.health_tips}
											</Text>
										</View>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() => this.props.navigation.dispatch(
                      StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'History'})]
                      })
                    )}
									>
										<View style={styles.itemContent}>
											<Icon name="list-alt" style={styles.iconStyle} />
											<Text style={styles.textStyle}>{strings.history}</Text>
										</View>
									</TouchableOpacity>
									<TouchableOpacity
										onPress={() => this.props.navigation.navigate("Settings")}
									>
										<View style={styles.itemContent}>
											<Icon name="cogs" style={styles.iconStyle} />
											<Text style={styles.textStyle}>{strings.settings}</Text>
										</View>
									</TouchableOpacity>
									<TouchableOpacity onPress={() => this.handleOnLogout()}>
										<View style={styles.itemContent}>
											<Icon name="sign-out" style={styles.iconStyle} />
											<Text style={styles.textStyle}>{strings.logout}</Text>
										</View>
									</TouchableOpacity>
								</View>
							</Content>
						</Container>
					)}
				</Subscribe>
			</Provider>
		);
	}
}

export default Sidebar;
