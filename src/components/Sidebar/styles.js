import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	iconStyle: {
		color: "#051a36",
		fontSize: 16,
		paddingTop: 2
	},
	textStyle: {
		fontSize: 16,
		marginLeft: 20
	},
	itemContent: {
		flexDirection: "row",
		alignItems: "center",
		paddingTop: 15,
		paddingVertical: 15
	},
	headerIcon: {
		padding: 20,
		fontSize: 70,
		color: "#fff"
	},
	titleFontStyle: {
		fontSize: 15,
		color: "#fff"
	}
});

export { styles };
