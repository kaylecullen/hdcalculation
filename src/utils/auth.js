import { AsyncStorage } from 'react-native';
export const TOKEN = "TOKEN";
export const UNAME = "userName";
export const RISK_LEVEL = "riskLevel";
export const PERCENTAGE_RISK = "cardioRisk";
export const REGISTER = "register";
export const LOGIN = 'login';

export const getStorageValue = async (key) => {
  const secretValue = "";
  await AsyncStorage.getItem(key).then((val) => {
    this.secretValue = val;
  })

  return this.secretValue;
};

export const setStorageValue = async (item, selectedValue) => {
  try {
    if(selectedValue){
      await AsyncStorage.setItem(item, selectedValue.toString());
    }
  } catch (error) {
    console.log('AsyncStorag1e error: ' + error.message);
    return false;
  }

  return true;
};