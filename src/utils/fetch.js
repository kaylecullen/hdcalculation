import { appContainer } from '../container';
import { getStorageValue, TOKEN } from '../utils/auth';

const jsonMimeType = "application/json";

export const fetchUrl = async (url, method = 'GET', body = null, contentType = jsonMimeType, limit = 5) => {
  const token = await getStorageValue(TOKEN).then(res => {return res});
 
  const request = new Request(url, {
    method: method,
    headers: {
      'Content-Type': contentType,
      Authorization: token ? 'Bearer '+token : '',
    },
    body: body
  });

  return fetch(request)
  .then(async response => {
    return response.json();
  })
  .then(res => {
    return res;
  })
  .catch(err => {
    console.log(err);
    appContainer.setState({ showError: true, errorMessage: err.message });

    return "error";
  })
 
}