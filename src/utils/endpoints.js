const API_HOST = "http://lifemed-001-site1.dtempurl.com";
// const API_HOST = "http://192.168.43.168:5555";

export const endpoint = {
	register: API_HOST + "/api/Account/Register/",
	healthTips: API_HOST + "/api/HealthTips/",
	login: API_HOST + "/token",
	heartCalculator: API_HOST + "/api/HeartComputation",
	userDetails: API_HOST + "/api/user",
	userHistory: API_HOST + "/api/User/History",
	userStatus: API_HOST + "/api/user/status",
  changePassword: API_HOST + "/api/account/changePassword",
  percentageLevel: API_HOST + "/api/HealthTips/Tips/",
};
