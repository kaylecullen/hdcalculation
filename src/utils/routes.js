import React from 'react';
import {
	createStackNavigator,
	createAppContainer,
	createDrawerNavigator,
  createSwitchNavigator,
  createBottomTabNavigator,
} from 'react-navigation';

import {
	Welcome,
	Login,
	Register,
	Guest,
	Result,
	Dashboard,
	HealthTips,
  History,
  Settings,
  Loading,
  GuestForm2,
  GuestForm3,
  GuestForm4,
} from '../screens';
import { Sidebar } from '../components/';
import { Left } from 'native-base';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { getStorageValue, TOKEN } from '../utils/auth';
import { appContainer } from '../container';
import { theme } from '../../theme';

const checkToken = getStorageValue(TOKEN).then(res => {
  if(res){
    return true;
  }
  return false;
})

const CalculatorTab = createBottomTabNavigator(
  {
    Guest: {
      screen: Guest,
      navigationOptions: ({navigation}) => ({
        tabBarVisible: false,
      })
    },
    GuestForm2: {
      screen: GuestForm2,
      navigationOptions: ({navigation}) => ({
        tabBarVisible: false,
      })
    },
    GuestForm3: {
      screen: GuestForm3,
      navigationOptions: ({navigation}) => ({
        tabBarVisible: false,
      })
    },
    GuestForm4: {
      screen: GuestForm4,
      navigationOptions: ({navigation}) => ({
        tabBarVisible: false,
      })
    },
    Result: {
      screen: Result,
      navigationOptions: ({navigation}) => ({
        tabBarVisible: false,
      })
		}
  },
  {
    initialRouteName: 'Guest',
    backBehavior: 'none',
  }
);

const Auth = createStackNavigator(
	{
		Welcome: {
			screen: Welcome
		},
		Login: {
			screen: Login
		},
		Register: {
			screen: Register
		},
    Guest: {
      screen: CalculatorTab,
      navigationOptions: ({ navigation }) => ({
        title: 'HD Calculator',
        headerStyle: {
          backgroundColor: theme.colors.navigation
        },
        headerTintColor: theme.colors.surface,
      })
    },
	},
	{
		initialRouteName: 'Welcome'
	}
);

const AppStack = createStackNavigator(
	{
		Dashboard: {
			screen: Dashboard
		},
		HealthTips: {
			screen: HealthTips
		},
		Guest: {
      screen: CalculatorTab,
      navigationOptions: ({ navigation }) => ({
        title: 'HD Calculator',
        headerStyle: {
          backgroundColor: theme.colors.navigation
        },
        headerTintColor: theme.colors.surface,
        headerLeft: (
          <Left>
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
              <Icon
                name="bars"
                style={{ color: "#fff", fontSize: 20, paddingLeft: 15 }}
              />
            </TouchableOpacity>
          </Left>
        )
      })
    },
    History: {
      screen: History
    },
    Settings: {
      screen: Settings,
    },
	},
	{
		initialRouteName: 'Dashboard',
		navigationOptions: ({ navigation }) => ({
			headerStyle: {
				backgroundColor: '#051a36'
			},
			headerTintColor: '#fff',
			headerLeft: (
				<Left>
					<TouchableOpacity onPress={() => navigation.openDrawer()}>
						<Icon
							name='bars'
							style={{ color: '#000', fontSize: 20, paddingLeft: 15 }}
						/>
					</TouchableOpacity>
				</Left>
			)
		})
	}
);

const App = createDrawerNavigator(
	{
		GuestUser: {
			screen: AppStack
		}
	},
	{
		drawerPosition: 'left',
		contentComponent: props => <Sidebar {...props} />
	}
);

const Navigation = createSwitchNavigator(
	{
    Loading: {
      screen: Loading
    },
		App: {
			screen: App
		},
		Auth: {
			screen: Auth
		}
	},
	{
		initialRouteName: 'Loading',
	}
);
const Navigator = createAppContainer(Navigation);

export default Navigator;
